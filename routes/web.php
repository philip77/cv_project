<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PersonalInformationController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ExperienceController;
use App\Http\Controllers\EducationController;
use App\Http\Controllers\CertificateController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ProjectInformationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PersonalInformationController::class, 'index'])->name('personal-information.index');
Route::get('/projects', [ProjectController::class, 'index'])->name('projects.index');
Route::get('/experience', [ExperienceController::class, 'index'])->name('experience.index');
Route::get('/education', [EducationController::class, 'index'])->name('education.index');
Route::get('/education', [EducationController::class, 'index'])->name('education.index');
Route::get('/certificates', [CertificateController::class, 'index'])->name('certificates.index');
Route::get('/message', [MessageController::class, 'create'])->name('message.create');
Route::post('/message-store', [MessageController::class, 'store'])->name('message.store');

Route::get('/experience-show-all', [ExperienceController::class, 'showAll'])->name('experience.show.all');
Route::get('/experience-create', [ExperienceController::class, 'create'])->name('experience.create');
Route::post('/experience-store', [ExperienceController::class, 'store'])->name('experience.store');
Route::get('/experience-edit/{experience}', [ExperienceController::class, 'edit'])->name('experience.edit');
Route::patch('/experience-update/{experience}', [ExperienceController::class, 'update'])->name('experience.update');

Route::get('/education-show-all', [EducationController::class, 'showAll'])->name('education.show.all');
Route::get('/education-create', [EducationController::class, 'create'])->name('education.create');
Route::post('/education-store', [EducationController::class, 'store'])->name('education.store');
Route::get('/education-edit/{education}', [EducationController::class, 'edit'])->name('education.edit');
Route::patch('/education-update/{education}', [EducationController::class, 'update'])->name('education.update');

Route::get('/personal-information-show-all', [PersonalInformationController::class, 'showAll'])->name('personal.information.show.all');
Route::get('/personal-information-create', [PersonalInformationController::class, 'create'])->name('personal.information.create');
Route::post('/personal-information-store', [PersonalInformationController::class, 'store'])->name('personal.information.store');
Route::get('/personal-information-edit/{personalInformation}', [PersonalInformationController::class, 'edit'])->name('personal.information.edit');
Route::patch('/personal-information-update/{personalInformation}', [PersonalInformationController::class, 'update'])->name('personal.information.update');

Route::get('/project-show-all', [ProjectController::class, 'showAll'])->name('project.show.all');
Route::get('/project-create', [ProjectController::class, 'create'])->name('project.create');
Route::post('/project-store', [ProjectController::class, 'store'])->name('project.store');
Route::get('/project-edit/{project}', [ProjectController::class, 'edit'])->name('project.edit');
Route::patch('/project-update/{project}', [ProjectController::class, 'update'])->name('project.update');

Route::get('/project-information-show-all/{project}', [ProjectInformationController::class, 'showAll'])->name('project.information.show.all');
Route::get('/project-information-create/{project}', [ProjectInformationController::class, 'create'])->name('project.information.create');
Route::post('/project-information-store/', [ProjectInformationController::class, 'store'])->name('project.information.store');
Route::get('/project-information-edit/{projectInformation}', [ProjectInformationController::class, 'edit'])->name('project.information.edit');
Route::patch('/project-information-update/{projectInformation}', [ProjectInformationController::class, 'update'])->name('project.information.update');

Route::get('/message-index', [MessageController::class, 'index'])->name('message.index');
Route::get('/message-show/{message}', [MessageController::class, 'show'])->name('message.show');

Route::get('/certificate-show-all', [CertificateController::class, 'showAll'])->name('certificate.show.all');
Route::get('/certificate-create', [CertificateController::class, 'create'])->name('certificate.create');
Route::post('/certificate-store', [CertificateController::class, 'store'])->name('certificate.store');
Route::get('/certificate-edit/{certificate}', [CertificateController::class, 'edit'])->name('certificate.edit');
Route::patch('/certificate-update/{certificate}', [CertificateController::class, 'update'])->name('certificate.update');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
