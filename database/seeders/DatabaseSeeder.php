<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Education;
use App\Models\Experience;
use App\Models\PersonalInformation;
use App\Models\Project;
use App\Models\ProjectInformation;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $dataPersonalInformation = [
            ['key' => 'Name', 'value' => 'Philip Georgiev Popov'],
            ['key' => 'Email', 'value' => 'philip_popov@philip-popov.eu'],
            ['key' => 'Phone', 'value' => '+359 878 647164'],
            ['key' => 'Date of birth', 'value' => '14 August 1984'],
            ['key' => 'Address', 'value' => 'bul. Treti Mart 6, Gabrovo, Bulgaria'],
            ['key' => 'Nationality', 'value' => 'Bulgarian'],
            ['key' => 'Personal Information', 'value' => 'I\'m web developer with 6 years professional experience. Proficient at PHP, JavaScript, SQL (MySQL, PostgreSQL, MsSQL), CSS, HTML.'],
        ];

        PersonalInformation::insert($dataPersonalInformation);

        $dataProjects = [
            ['name' => 'Midgard'],
            ['name' => 'Shopping Cart'],
            ['name' => 'My Page'],
            ['name' => 'Diploma KST'],
        ];

        Project::insert($dataProjects);

        $dataProjectInformation = [
            ['project_id' => 1, 'key' => 'Name', 'value' => 'Midgard (MMORPG game)', 'link' => null],
            ['project_id' => 1, 'key' => 'Link for GitHub', 'value' => 'To GitHub', 'link' => 'https://github.com/filippopov/Midgard'],
            ['project_id' => 1, 'key' => 'Link for Project', 'value' => 'Midgard (MMORPG game)', 'link' => 'http://midgard.philip-popov.eu/'],
            ['project_id' => 1, 'key' => 'Technologies', 'value' => 'PHP 7 (my custom MVC framework), JavaScript, MySQL, jQuery, HTML, CSS and Bootstrap.', 'link' => null],
            ['project_id' => 2, 'key' => 'Name', 'value' => 'Shopping Cart', 'link' => null],
            ['project_id' => 2, 'key' => 'Link for GitHub', 'value' => 'To GitHub', 'link' => 'https://github.com/filippopov/ShoppingCartSymfony'],
            ['project_id' => 2, 'key' => 'Technologies', 'value' => 'PHP 7 (Symfony framework), MySQL, JavaScript, jQuery, HTML, CSS and Bootstrap.', 'link' => null],
            ['project_id' => 3, 'key' => 'Name', 'value' => 'My Page', 'link' => null],
            ['project_id' => 3, 'key' => 'Link for GitHub', 'value' => 'To GitHub', 'link' => 'https://github.com/filippopov/css_advanced'],
            ['project_id' => 3, 'key' => 'Link for Project', 'value' => 'To My Page', 'link' => 'https://philip-popov.eu'],
            ['project_id' => 3, 'key' => 'Technologies', 'value' => 'Jekyll, CSS, SASS, HTML, JavaScript and jQuery', 'link' => null],
            ['project_id' => 4, 'key' => 'Name', 'value' => 'Historical Sites (Diploma KST)', 'link' => null],
            ['project_id' => 4, 'key' => 'Link for GitHub', 'value' => 'To GitLab', 'link' => 'https://gitlab.com/philip77/diploma_kst'],
            ['project_id' => 4, 'key' => 'Link for Project', 'value' => 'To My Page', 'link' => 'http://historical-sites.philip-popov.eu/'],
            ['project_id' => 4, 'key' => 'Technologies', 'value' => 'Laravel 10, PHP 8.2, MySQL, JavaScript, jQuery, HTML, CSS', 'link' => null],
        ];

        ProjectInformation::insert($dataProjectInformation);

        $dataExperience = [
            ['from' => (new \DateTime('2012-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2013-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Support engineer', 'data' => 'Dartek OOD, Ruse (Bulgaria)'],
            ['from' => (new \DateTime('2013-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2013-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Mechatronics', 'data' => 'Montupe EOOD, Ruse (Bulgaria)'],
            ['from' => (new \DateTime('2013-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2013-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Seller', 'data' => 'Masater OOD, Ruse (Bulgaria)'],
            ['from' => (new \DateTime('2013-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2015-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Seller', 'data' => 'Technomarket, Ruse (Bulgaria)'],
            ['from' => (new \DateTime('2016-01-17'))->format('Y-m-d'), 'to' => (new \DateTime('2016-11-30'))->format('Y-m-d'), 'date_format' => 'd/m/Y', 'name' => 'Junior developer', 'data' => 'XAPT solutions, Sofia (Bulgaria)'],
            ['from' => (new \DateTime('2017-02-05'))->format('Y-m-d'), 'to' => (new \DateTime('2018-04-13'))->format('Y-m-d'), 'date_format' => 'd/m/Y', 'name' => 'Website developer', 'data' => 'Support services EOOD, Sofia (Bulgaria)'],
            ['from' => (new \DateTime('2018-04-16'))->format('Y-m-d'), 'to' => (new \DateTime('2018-11-12'))->format('Y-m-d'), 'date_format' => 'd/m/Y', 'name' => 'Website developer', 'data' => 'Ytuitive EOOD, Sofia (Bulgaria)'],
            ['from' => (new \DateTime('2018-11-19'))->format('Y-m-d'), 'to' => (new \DateTime('2021-04-24'))->format('Y-m-d'), 'date_format' => 'd/m/Y', 'name' => 'Website developer', 'data' => 'Enetpulse EOOD, Sofia (Bulgaria)'],
            ['from' => (new \DateTime('2021-04-26'))->format('Y-m-d'), 'to' => (new \DateTime('2021-06-19'))->format('Y-m-d'), 'date_format' => 'd/m/Y', 'name' => 'Drupal developer', 'data' => 'Bulcode 2016 OOD, Sofia (Bulgaria)'],
        ];

        Experience::insert($dataExperience);

        $dataEducation = [
            ['from' => (new \DateTime('2003-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2009-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Bachelor Electricity and Electrical Equipment', 'data' => 'Rousse University Angel Kanchev, Ruse (Bulgaria)'],
            ['from' => (new \DateTime('2011-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2012-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Master Electronics', 'data' => 'Rousse University Angel Kanchev, Ruse (Bulgaria)'],
            ['from' => (new \DateTime('2014-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2016-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Practical Software Engineering', 'data' => 'Software University, Sofia (Bulgaria)'],
            ['from' => (new \DateTime('2022-01-01'))->format('Y-m-d'), 'to' => (new \DateTime('2023-01-01'))->format('Y-m-d'), 'date_format' => 'Y', 'name' => 'Master Computer Systems and Technology', 'data' => 'Technical University of Gabrovo, Gabrovo (Bulgaria)'],
        ];

        Education::insert($dataEducation);
    }
}
