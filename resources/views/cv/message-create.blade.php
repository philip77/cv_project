<x-cv-layout>
    <section>
        <header>
            <h2>
                Contact
            </h2>
        </header>
        <div class="send-message">
            <form action="{{ route('message.store') }}" method="post">
                @csrf

                <fieldset>
                    <legend>DROP ME A LINE</legend>
                    <p class="field">
                        <span class="input">
                            <input type="text" name="name" value="{{ old('name') }}" placeholder="Your Name">
                            <span class="actions"></span>
                            <i class="fas fa-user"></i>
                        </span>
                    </p>
                    @if($errors->get('name'))
                        <ul class="errors">
                            @foreach ($errors->get('name') as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <p class="field">
                        <span class="input">
                            <input type="text" name="email" value="{{ old('email') }}"  placeholder="Your Email">
                            <span class="actions"></span>
                            <i class="fas fa-envelope"></i>
                        </span>
                    </p>
                    @if($errors->get('email'))
                        <ul class="errors">
                            @foreach ($errors->get('email') as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <p class="field">
                        <span class="input">
                            <textarea name="message" cols="30" rows="10" placeholder="Your Message...">{{ old('message') }}</textarea>
                            <span class="actions"></span>
                        </span>
                    </p>
                    @if($errors->get('message'))
                        <ul class="errors">
                            @foreach ($errors->get('message') as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <input type="submit" value="SAY HELLO!">
                </fieldset>
            </form>
        </div>
    </section>
</x-cv-layout>
