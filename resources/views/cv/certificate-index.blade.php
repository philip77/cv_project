<x-cv-layout>
    <section>
        <header>
            <h2>
                Certificates
            </h2>
        </header>
        <ul class="gallery">

            <li><a href="#"><img src="{{ asset('storage/images/Diploma%20-%20Diploma.jpeg') }}" alt="Diploma SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/PHP%20Fundamentals%20-%20January%202017%20-%20Certificate.jpeg') }}" alt="PHP Fundamentals SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/PHP%20Web%20Development%20-%20October%202016%20-%20Certificate.jpeg') }}" alt="PHP WEB Development SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/JS%20Fundamentals%20-%20May%202018%20-%20Certificate.jpeg') }}" alt="JS Fundamentals SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/JS%20Advanced%20-%20June%202018%20-%20Certificate.jpeg') }}" alt="JS Advanced SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/JS%20Applications%20-%20July%202018%20-%20Certificate.jpeg') }}" alt="JS Application SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/JS%20Frameworks%20with%20AngularJS%20-%20May%202015%20-%20Certificate.jpeg') }}" alt="JS Framework SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/Web%20Fundamentals%20-%20HTML5%20-%20September%202018%20-%20Certificate.jpeg') }}" alt="WEB Fundamentals SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/Drupal%20Site%20Building%20-%20September%202015%20-%20Certificate.jpeg') }}" alt="Drupal Site Building SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/Advanced%20C%23%20-%20February%202016%20-%20Certificate.jpeg') }}" alt="Advanced C# SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/SoftUniada%20-%20January%202016%20-%20Certificate.jpeg') }}" alt="SoftUniada SoftUni"></a></li>
            <li><a href="#"><img src="{{ asset('storage/images/Teamwork%20and%20Personal%20Skills%20-%20Sep%202014%20-%20Certificate.jpeg')}}" alt="Teamwork and Personal Skills"></a></li>

            @foreach($certificates as $certificate)
                <li><a href="#"><img src="{{ asset('storage/images/' . $certificate->image)}}" alt="{{ $certificate->name }}"></a></li>
            @endforeach
        </ul>
    </section>
    <div id="dialog-form" title="Certificates">

    </div>
</x-cv-layout>


<script>
    $(function () {
        dialog = $( "#dialog-form" ).dialog({
            autoOpen: false,
            height: 725,
            width: 725,
            buttons: {
                Cancel: function() {
                    dialog.dialog('close');
                }
            }
        });

        $('.gallery li').on( "click", function() {
            dialog.dialog( "open" );
            $('#dialog-form').empty();
            let imageSrc = $(this).children().children().attr('src');
            $('#dialog-form').append($('<img>').attr('src', imageSrc));
        });
    });
</script>
