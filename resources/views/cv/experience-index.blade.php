<x-cv-layout>
    <section>
        <header>
            <h2>
                Experience
            </h2>
        </header>
        <div class="my-experience">
            @foreach($experiences as $experience)
                <div>
                    <div>
                        {{ $experience->from }}-{{ $experience->to }}
                    </div>
                    <div>
                        <p>{{ $experience->name }}</p>
                        <p>{{ $experience->data }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
</x-cv-layout>
