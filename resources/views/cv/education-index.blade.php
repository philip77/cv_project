<x-cv-layout>
    <section>
        <header>
            <h2>
                Education
            </h2>
        </header>
        <div class="my-experience">
            @foreach($educations as $education)
                <div>
                    <div>
                        {{ $education->from }}-{{ $education->to }}
                    </div>
                    <div>
                        <p>{{ $education->name }}</p>
                        <p>{{ $education->data }}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
</x-cv-layout>
