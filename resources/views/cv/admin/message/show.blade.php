<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show massage') }}
        </h2>
    </x-slot>
    <div class="message-container">
        <div id="button-container">
            <a id="app-button" href="{{ route('message.index') }}" class="button">Back</a>
        </div>
        <h3>Name: {{ $message->name }}</h3>
        <h3>Email: {{ $message->email }}</h3>
        <p> Message: {{ $message->message }}</p>
    </div>
</x-app-layout>
