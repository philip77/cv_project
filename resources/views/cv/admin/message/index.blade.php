<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show my massages') }}
        </h2>
    </x-slot>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Message</th>
            <th>Show</th>
        </tr>
        </thead>
        <tbody>
        @foreach($messages as $message)
            <tr>
                <td data-label="Id">{{ $message->id }}</td>
                <td data-label="Name">{{ $message->name }}</td>
                <td data-label="Email">{{ $message->email }}</td>
                <td data-label="Message">{{ Str::limit($message->message, 50, $end = '...') }}</td>
                <td data-label="Show">
                    <a id="table-button" href="{{ route('message.show', [$message->id]) }}" >Show</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div id="pagination">
        {{ $messages->links() }}
    </div>

    <div id="button-container">
        <a id="app-button" href="{{ route('dashboard') }}" class="button">Back</a>
    </div>
</x-app-layout>
