<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show my personal information') }}
        </h2>
    </x-slot>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>For Project</th>
            <th>Key</th>
            <th>Value</th>
            <th>Link</th>
            <th>Is Deleted</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($projectInformationRows as $projectInformation)
            <tr>
                <td data-label="Id">{{ $projectInformation->id }}</td>
                <td data-label="For Project">{{ $project->name }}</td>
                <td data-label="Key">{{ $projectInformation->key }}</td>
                <td data-label="Value">{{ $projectInformation->value }}</td>
                <td data-label="Link">{{ $projectInformation->link }}</td>
                <td data-label="Is Deleted">{{ $projectInformation->is_deleted ? 'Yes' : 'No' }}</td>
                <td data-label="Edit">
                    <a id="table-button" href="{{ route('project.information.edit', [$projectInformation->id]) }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="button-container">
        <a id="app-button" href="{{ route('project.show.all') }}" class="button">Back</a>
        <a id="app-button" href="{{ route('project.information.create', $project->id) }}" class="button">Create</a>
    </div>
</x-app-layout>
