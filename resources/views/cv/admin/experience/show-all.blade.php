<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show my experience') }}
        </h2>
    </x-slot>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>From</th>
            <th>To</th>
            <th>Date Format</th>
            <th>Name</th>
            <th>Data</th>
            <th>Is-Deleted</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($experiences as $experience)
            <tr>
                <td data-label="Id">{{ $experience->id }}</td>
                <td data-label="From">{{ $experience->from }}</td>
                <td data-label="To">{{ $experience->to }}</td>
                <td data-label="Date Format">{{ $experience->date_format }}</td>
                <td data-label="Name">{{ $experience->name }}</td>
                <td data-label="Data">{{ Str::limit($experience->data, 50, $end = '...') }}</td>
                <td data-label="Is Deleted">{{ $experience->is_deleted ? 'Yes' : 'No' }}</td>
                <td data-label="Edit">
                    <a id="table-button" href="{{ route('experience.edit', [$experience->id]) }}" >Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="button-container">
        <a id="app-button" href="{{ route('dashboard') }}" class="button">Back</a>
        <a id="app-button" href="{{ route('experience.create') }}" class="button">Create</a>
    </div>
</x-app-layout>
