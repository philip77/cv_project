<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show my personal information') }}
        </h2>
    </x-slot>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Key</th>
            <th>Value</th>
            <th>Is Deleted</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($personalInformationRows as $personalInformation)
            <tr>
                <td data-label="Id">{{ $personalInformation->id }}</td>
                <td data-label="Key">{{ $personalInformation->key }}</td>
                <td data-label="Value">{{ $personalInformation->value }}</td>
                <td data-label="Is Deleted">{{ $personalInformation->is_deleted ? 'Yes' : 'No' }}</td>
                <td data-label="Edit">
                    <a id="table-button" href="{{ route('personal.information.edit', [$personalInformation->id]) }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="button-container">
        <a id="app-button" href="{{ route('dashboard') }}" class="button">Back</a>
        <a id="app-button" href="{{ route('personal.information.create') }}" class="button">Create</a>
    </div>
</x-app-layout>
