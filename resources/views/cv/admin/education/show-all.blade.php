<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show my educations') }}
        </h2>
    </x-slot>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>From</th>
            <th>To</th>
            <th>Date Format</th>
            <th>Name</th>
            <th>Data</th>
            <th>Is-Deleted</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($educations as $education)
            <tr>
                <td data-label="Id">{{ $education->id }}</td>
                <td data-label="From">{{ $education->from }}</td>
                <td data-label="To">{{ $education->to }}</td>
                <td data-label="Date Format">{{ $education->date_format }}</td>
                <td data-label="Name">{{ $education->name }}</td>
                <td data-label="Data">{{ Str::limit($education->data, 50, $end = '...') }}</td>
                <td data-label="Is Deleted">{{ $education->is_deleted ? 'Yes' : 'No' }}</td>
                <td data-label="Edit">
                    <a id="table-button" href="{{ route('education.edit', [$education->id]) }}" >Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="button-container">
        <a id="app-button" href="{{ route('dashboard') }}" class="button">Back</a>
        <a id="app-button" href="{{ route('education.create') }}" class="button">Create</a>
    </div>
</x-app-layout>
