<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Edit education data') }}
        </h2>
    </x-slot>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-6">
        <a id="app-button" href="{{ route('education.show.all') }}" class="button">Back</a>
        <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
            <div class="max-w-xl">
                <section>
                    <form method="post" action="{{ route('education.update', [$education->id]) }}" class="mt-6 space-y-6">
                        @csrf
                        @method('patch')

                        <div>
                            <x-input-label for="from" :value="__('From')" />
                            <x-text-input class="datepicker" id="from" name="from" type="text" class="mt-1 block w-full" :value="old('from', $education->from)" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('from')" />
                        </div>

                        <div>
                            <x-input-label for="to" :value="__('To')" />
                            <x-text-input class="datepicker" id="to" name="to" type="text" class="mt-1 block w-full" :value="old('to', $education->to)" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('to')" />
                        </div>

                        <div>
                            <x-input-label for="date-format" :value="__('Date Format')" />
                            <select id="date-format" name="date_format" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full" autofocus>
                                <option value="Y" {{ old('date_format', $education->date_format) == 'Y' ? 'selected' : '' }}>Y</option>
                                <option value="d/m/Y" {{ old('date_format', $education->date_format) == 'd/m/Y' ? 'selected' : '' }}>d/m/Y</option>
                            </select>
                            <x-input-error class="mt-2" :messages="$errors->get('date_format')" />
                        </div>

                        <div>
                            <x-input-label for="name" :value="__('Name')" />
                            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $education->name)" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('name')" />
                        </div>

                        <div>
                            <x-input-label for="data" :value="__('Data')" />
                            <textarea id="data" name="data" class="mt-1 block w-full" autofocus>{{ old('data', $education->data) }}</textarea>
                            <x-input-error class="mt-2" :messages="$errors->get('data')" />
                        </div>

                        <div>
                            <x-input-label for="is-deleted" :value="__('Is Deleted')" />
                            <select id="is-deleted" name="is_deleted" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full" autofocus>
                                <option value="0" {{ old('is_deleted', $education->is_deleted) == '0' ? 'selected' : '' }}>No</option>
                                <option value="1" {{ old('is_deleted', $education->is_deleted) == '1' ? 'selected' : '' }}>Yes</option>
                            </select>
                            <x-input-error class="mt-2" :messages="$errors->get('is_deleted')" />
                        </div>

                        <div class="flex items-center gap-4">
                            <x-primary-button>{{ __('Save') }}</x-primary-button>

                            @if (session('status') === 'historical-site-store')
                                <p
                                    x-data="{ show: true }"
                                    x-show="show"
                                    x-transition
                                    x-init="setTimeout(() => show = false, 2000)"
                                    class="text-sm text-gray-600 dark:text-gray-400"
                                >{{ __('Saved.') }}</p>
                            @endif

                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</x-app-layout>

<script>
    $(function() {
        $("#from").datepicker({
            minDate: '-120Y',
            maxDate: '0',
            changeMonth: true,
            changeYear: true
        });

        $("#to").datepicker({
            minDate: '-120Y',
            maxDate: '0',
            changeMonth: true,
            changeYear: true
        });
    });
</script>

