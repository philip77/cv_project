<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show my certificates') }}
        </h2>
    </x-slot>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Image Path</th>
            <th>Is Deleted</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($certificates as $certificate)
            <tr>
                <td data-label="Id">{{ $certificate->id }}</td>
                <td data-label="Name">{{ $certificate->name }}</td>
                <td data-label="Image Path">{{ $certificate->image }}</td>
                <td data-label="Is Deleted">{{ $certificate->is_deleted ? 'Yes' : 'No' }}</td>
                <td data-label="Edit">
                    <a id="table-button" href="{{ route('certificate.edit', [$certificate->id]) }}" >Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div id="button-container">
        <a id="app-button" href="{{ route('dashboard') }}" class="button">Back</a>
        <a id="app-button" href="{{ route('certificate.create') }}" class="button">Add</a>
    </div>
</x-app-layout>
