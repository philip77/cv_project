<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Edit certificate') }}
        </h2>
    </x-slot>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-6">
        <a id="app-button" href="{{ route('certificate.show.all') }}" class="button">Back</a>
        <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
            <div class="max-w-xl">
                <section>
                    <form method="post" enctype="multipart/form-data"  action="{{ route('certificate.update', $certificate->id) }}" class="mt-6 space-y-6">
                        @csrf
                        @method('patch')

                        <div>
                            <x-input-label for="name" :value="__('Name')" />
                            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $certificate->name)" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('name')" />
                        </div>

                        <div>
                            <x-input-label for="image" :value="__('Image')" />
                            <image src="{{ asset('storage/images/' . $certificate->image) }}"></image>
                            <x-text-input id="image" name="image" type="file" class="mt-1 block w-full" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('image')" />
                        </div>

                        <div>
                            <x-input-label for="is-deleted" :value="__('Is Deleted')" />
                            <select id="is-deleted" name="is_deleted" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full" autofocus>
                                <option value="0" {{ old('is_deleted', $certificate->is_deleted) == '0' ? 'selected' : '' }}>No</option>
                                <option value="1" {{ old('is_deleted', $certificate->is_deleted) == '1' ? 'selected' : '' }}>Yes</option>
                            </select>
                            <x-input-error class="mt-2" :messages="$errors->get('is_deleted')" />
                        </div>

                        <div class="flex items-center gap-4">
                            <x-primary-button>{{ __('Save') }}</x-primary-button>

                            @if (session('status') === 'historical-site-store')
                                <p
                                    x-data="{ show: true }"
                                    x-show="show"
                                    x-transition
                                    x-init="setTimeout(() => show = false, 2000)"
                                    class="text-sm text-gray-600 dark:text-gray-400"
                                >{{ __('Saved.') }}</p>
                            @endif

                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</x-app-layout>
