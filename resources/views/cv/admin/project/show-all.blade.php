<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Show my projects') }}
        </h2>
    </x-slot>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Is-Deleted</th>
            <th>Project Information</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            <tr>
                <td data-label="Id">{{ $project->id }}</td>
                <td data-label="Name">{{ $project->name }}</td>
                <td data-label="Is Deleted">{{ $project->is_deleted ? 'Yes' : 'No' }}</td>
                <td data-label="Project Information">
                    <a id="table-button" href="{{ route('project.information.show.all', $project->id) }}" >Project Information</a>
                </td>
                <td data-label="Edit">
                    <a id="table-button" href="{{ route('project.edit', [$project->id]) }}" >Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="button-container">
        <a id="app-button" href="{{ route('dashboard') }}" class="button">Back</a>
        <a id="app-button" href="{{ route('project.create') }}" class="button">Create</a>
    </div>
</x-app-layout>
