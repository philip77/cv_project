<x-cv-layout>
    <section>
        <header>
            <h2>
                Projects
            </h2>
        </header>
        <div class="project-information">
            @foreach($projects as $project)
                <div>
                    @foreach($project->projectInformation as $row)
                        @if ($row->link)
                            <div><span>{{ $row->key }}: </span> <a target="_blank" href="{{ $row->link }}">{{ $row->value }}</a></div>
                        @else
                            <div><span>{{ $row->key }}: </span>{{ $row->value }}</div>
                        @endif
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
</x-cv-layout>
