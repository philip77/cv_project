<x-cv-layout>
    <section>
        <header>
            <h2>
                Home
            </h2>
        </header>
        <div class="my-information">
            @foreach($personalInformation as $row)
                <div>
                    <span>{{ $row->key }}:</span> {{ $row->value }}
                </div>
            @endforeach
        </div>
        @if($personalInformationRow)
            <div class="personal-information">
                <h4>{{ $personalInformationRow->key }}</h4>
                <p>
                    {{ $personalInformationRow->value }}
                </p>
            </div>
        @endif
    </section>
</x-cv-layout>
