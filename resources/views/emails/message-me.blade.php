<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title>Message Me</title>
    </head>
    <body>
        /* @var MessageService $messengerService */
        <h1>Name: {{ $messengerService->getName() }}</h1>
        <p>Email: {{ $messengerService->getEmail() }}</p>
        <p>
            {{ $messengerService->getMessage() }}
        </p>
    </body>
</html>
