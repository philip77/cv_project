
<nav class="navigation main">
    <ul>
        <li><a class="@php echo request()->routeIs('personal-information.index') ? 'active' : 'no-active'; @endphp" href="{{ route('personal-information.index') }}">Home</a></li>
        <li><a class="@php echo request()->routeIs('projects.index') ? 'active' : 'no-active'; @endphp" href="{{ route('projects.index') }}">Projects</a></li>
        <li><a class="@php echo request()->routeIs('experience.index') ? 'active' : 'no-active'; @endphp" href="{{ route('experience.index') }}">Experience</a></li>
        <li><a class="@php echo request()->routeIs('education.index') ? 'active' : 'no-active'; @endphp" href="{{ route('education.index') }}">Education</a></li>
        <li><a class="@php echo request()->routeIs('certificates.index') ? 'active' : 'no-active'; @endphp" href="{{ route('certificates.index') }}">Certificates</a></li>
        <li><a class="@php echo request()->routeIs('message.create') ? 'active' : 'no-active'; @endphp" href="{{ route('message.create') }}">Message Me</a></li>
    </ul>
</nav>
