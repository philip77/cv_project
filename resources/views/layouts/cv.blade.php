<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        {!! \Artesaos\SEOTools\Facades\SEOMeta::generate() !!}
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

{{--        <title>Philip Popov</title>--}}
        <link rel="shortcut icon" href="{{ asset('storage/images/favicon.ico') }}">

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <link href="{{ asset('js/jquery-ui/dist/themes/base/jquery-ui.min.css') }}" rel="stylesheet" />
        @vite(['resources/scss/styles.scss'])

        <script src="{{ asset('js/jquery-ui/external/jquery-3.6.0/jquery.js') }}"></script>
        <script src="{{ asset('js/jquery-ui/dist/jquery-ui.min.js') }}"></script>
    </head>
    <body class="antialiased">
        <input type="checkbox" id="nav-main-toggle" name="nav-main-toggle">
        <header class="site-header">
            <div class="wrapper">
                <div class="site-branding">
                    <p class="site-title">
                        <a href="/">Philip Popov</a>
                    </p>
                </div>
                <label id="nav-toggle" for="nav-main-toggle" class="fas nav-toggle left"></label>
            </div>
            @include('layouts.cv-navigation')

        </header>
        <main>
            <x-flash-success/>
            <x-flash-error/>
            {{ $slot }}
        </main>

        <footer class="site-footer">
            <div class="social">
                <a class="fab fa-github" href="https://github.com/filippopov" target="_blank"></a>
                <a class="fab fa-linkedin-in" href="https://www.linkedin.com/" target="_blank"></a>
                <a class="fab fa-facebook-f" href="https://www.facebook.com/filip.popov.5"  target="_blank"></a>
            </div>
            <div class="author">
                © Philip Popov.<br>
                All Rights Reserved.
            </div>
        </footer>
    </body>
</html>


