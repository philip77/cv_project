@if (session()->has('success'))
    <div id="success-message">
        <p>{{ session()->get('success') }}</p>
    </div>
@endif

<script>
    setTimeout(() => {
        $('#success-message').remove();
    }, 4000);
</script>
