@if (session()->has('error'))
    <div id="error-message">
        <p>{{ session()->get('error') }}</p>
    </div>
@endif

<script>
    setTimeout(() => {
        $('#error-message').remove();
    }, 4000);
</script>
