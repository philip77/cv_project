<?php

namespace App\Enum;

enum DateFormat: string
{
    case ONLY_YEAR = 'Y';
    case FULL_DATE = 'd/m/Y';
}
