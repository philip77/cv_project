<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Experience extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getFromAttribute(): string
    {
        $format = $this->attributes['date_format'] ?? 'd/m/Y';
        if (request()->routeIs('experience.edit')) {
            $format = 'm/d/Y';
        }

        return $this->attributes['from'] ? Carbon::createFromFormat('Y-m-d', $this->attributes['from'])->format($format) : '';
    }

    public function getToAttribute(): string
    {
        $format = $this->attributes['date_format'] ?? 'd/m/Y';
        if (request()->routeIs('experience.edit')) {
            $format = 'm/d/Y';
        }

        return $this->attributes['to'] ? Carbon::createFromFormat('Y-m-d', $this->attributes['to'])->format($format) : '';
    }

    public function setFromAttribute(?string $value)
    {
        $this->attributes['from'] = $value ? Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d') : null;
    }

    public function setToAttribute(?string $value)
    {
        $this->attributes['to'] = $value ? Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d') : null;
    }
}
