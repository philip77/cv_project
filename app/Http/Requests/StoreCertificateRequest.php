<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreCertificateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', Rule::unique('certificates', 'name')],
            'image' => ['required', 'image', 'mimes:jpg,png,jpeg,gif,svg', 'max:10048'],
            'is_deleted' => ['required', Rule::in(['0', '1'])],
        ];
    }
}
