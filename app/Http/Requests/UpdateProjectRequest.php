<?php

namespace App\Http\Requests;

use App\Models\Project;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProjectRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $project = Project::where('name', $this->request->get('name'))->first();
        return [
            'name' => ['required', 'string', Rule::unique('projects', 'name')->ignore($project->id ?? 0)],
            'is_deleted' => ['required', Rule::in(['0', '1'])],
        ];
    }
}
