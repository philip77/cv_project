<?php

namespace App\Http\Requests;

use App\Models\Certificate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCertificateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $certificate = Certificate::where('name', $this->request->get('name'))->first();
        return [
            'name' => ['nullable', 'string', Rule::unique('certificates', 'name')->ignore($certificate->id ?? 0)],
            'image' => ['nullable', 'image', 'mimes:jpg,png,jpeg,gif,svg', 'max:10048'],
            'is_deleted' => ['required', Rule::in(['0', '1'])],
        ];
    }
}
