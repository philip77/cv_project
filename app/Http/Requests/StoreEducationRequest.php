<?php

namespace App\Http\Requests;

use App\Enum\DateFormat;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class StoreEducationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'from' => ['required', 'date'],
            'to' => ['required', 'date'],
            'date_format' => ['required', new Enum(DateFormat::class)],
            'name' => ['required', 'string'],
            'data' => ['required', 'string'],
            'is_deleted' => ['required', Rule::in(['0', '1'])],
        ];
    }
}
