<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreExperienceRequest;
use App\Http\Requests\UpdateExperienceRequest;
use App\Models\Experience;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Redirect;

class ExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        SEOMeta::setTitle('My Experience');

        $experiences = Experience::where('is_deleted', 0)->orderBy('id', 'DESC')->get();

        return view('cv.experience-index', ['experiences' => $experiences]);
    }

    public function showAll()
    {
        $this->authorize('viewAny', Experience::class);

        return view('cv.admin.experience.show-all', ['experiences' => Experience::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('create', Experience::class);

        return view('cv.admin.experience.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreExperienceRequest $request)
    {
        try {
            $this->authorize('create', Experience::class);
            $validateData = $request->validated();

            Experience::create($validateData);

            return Redirect::route('experience.show.all')->with('success', 'Successfully add experience data!');
        } catch (\Exception $e) {
            return Redirect::route('experience.create')->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Experience $experience)
    {
        $this->authorize('update', $experience);

        return view('cv.admin.experience.edit', ['experience' => $experience]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateExperienceRequest $request, Experience $experience)
    {
        try {
            $this->authorize('update', $experience);

            $validateData = $request->validated();
            $experience->update($validateData);

            return Redirect::route('experience.show.all')->with('success', 'Successfully update experience data!');
        } catch (\Exception $e) {
            return Redirect::route('experience.edit', $experience->id)->with('error', $e->getMessage());
        }
    }
}
