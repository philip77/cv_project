<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\Project;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Redirect;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        SEOMeta::setTitle('My Projects');

        $projects = Project::with(['projectInformation' => function($query) {
            $query->where('is_deleted', 0);
        }])->where('is_deleted', 0)->get();

        return view('cv.project-index', ['projects' => $projects]);
    }

    public function showAll()
    {
        $this->authorize('viewAny', Project::class);

        return view('cv.admin.project.show-all', ['projects' => Project::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('create', Project::class);

        return view('cv.admin.project.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request)
    {
        try {
            $this->authorize('create', Project::class);
            $validateData = $request->validated();

            Project::create($validateData);

            return Redirect::route('project.show.all')->with('success', 'Successfully add project information data!');
        } catch (\Exception $e) {
            return Redirect::route('project.create')->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Project $project)
    {
        $this->authorize('update', $project);

        return view('cv.admin.project.edit', ['project' => $project]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        try {
            $this->authorize('update', $project);

            $validateData = $request->validated();
            $project->update($validateData);

            return Redirect::route('project.show.all')->with('success', 'Successfully update project data!');
        } catch (\Exception $e) {
            return Redirect::route('project.edit', $project->id)->with('error', $e->getMessage());
        }
    }
}
