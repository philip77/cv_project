<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePersonalInformationRequest;
use App\Http\Requests\UpdatePersonalInformationRequest;
use App\Models\PersonalInformation;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Redirect;

class PersonalInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        SEOMeta::setTitle('Personal Information');

        $personalInformation = PersonalInformation::where([['is_deleted', '=', '0'], ['key', '<>', 'Personal Information']])->orderBy('id', 'ASC')->get();
        $personalInformationRow = PersonalInformation::where([['key', '=', 'Personal Information'], ['is_deleted', '=', '0']])->first();

        return view('cv.personal-information-index', [
            'personalInformation' => $personalInformation,
            'personalInformationRow' => $personalInformationRow
        ]);
    }

    public function showAll()
    {
        $this->authorize('viewAny', PersonalInformation::class);

        return view('cv.admin.personal-information.show-all', ['personalInformationRows' => PersonalInformation::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('create', PersonalInformation::class);

        return view('cv.admin.personal-information.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePersonalInformationRequest $request)
    {
        try {
            $this->authorize('create', PersonalInformation::class);
            $validateData = $request->validated();

            PersonalInformation::create($validateData);

            return Redirect::route('personal.information.show.all')->with('success', 'Successfully add personal information data!');
        } catch (\Exception $e) {
            return Redirect::route('personal.information.create')->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PersonalInformation $personalInformation)
    {
        $this->authorize('update', $personalInformation);

        return view('cv.admin.personal-information.edit', ['personalInformation' => $personalInformation]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePersonalInformationRequest $request, PersonalInformation $personalInformation)
    {
        try {
            $this->authorize('update', $personalInformation);

            $validateData = $request->validated();
            $personalInformation->update($validateData);

            return Redirect::route('personal.information.show.all')->with('success', 'Successfully update personal information data!');
        } catch (\Exception $e) {
            return Redirect::route('personal.information.edit', $personalInformation->id)->with('error', $e->getMessage());
        }
    }
}
