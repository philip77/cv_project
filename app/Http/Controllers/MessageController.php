<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessageRequest;
use App\Http\Requests\UpdateMessageRequest;
use App\Http\Services\MessengerService;
use App\Mail\Messenger;
use App\Models\Message;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('viewAny', Message::class);

        return view('cv.admin.message.index', ['messages' => Message::latest('id')->paginate(10)->withQueryString()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        SEOMeta::setTitle('Message Me');

        return view('cv.message-create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMessageRequest $request)
    {
        try {
            $validateData = $request->validated();
            Message::create($validateData);

            $messageService = new MessengerService($validateData['name'], $validateData['email'], $validateData['message']);
//            Mail::to('philip_popov@philip-popov.eu')->send(new Messenger($messageService));

            return Redirect::route('message.create')->with('success', 'You successfully send me a message!');
        } catch (\Exception $e) {
            return Redirect::route('message.create')->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Message $message)
    {
        $this->authorize('view', $message);

        return view('cv.admin.message.show', ['message' => $message]);
    }
}
