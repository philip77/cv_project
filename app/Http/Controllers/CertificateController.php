<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCertificateRequest;
use App\Http\Requests\UpdateCertificateRequest;
use App\Models\Certificate;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Redirect;

class CertificateController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle('My Certificates');

        return view('cv.certificate-index', ['certificates' => Certificate::where('is_deleted', 0)->orderBy('id', 'ASC')->get()]);
    }

    public function showAll()
    {
        $this->authorize('viewAny', Certificate::class);

        return view('cv.admin.certificate.show-all', ['certificates' => Certificate::all()]);
    }

    public function create()
    {
        $this->authorize('create', Certificate::class);

        return view('cv.admin.certificate.create');
    }

    public function store(StoreCertificateRequest $request)
    {
        try {
            $this->authorize('create', Certificate::class);
            $validateData = $request->validated();

            $image = uniqid() . '_' . $request->image->getClientOriginalName();
            $request->image->move(public_path('storage/images'), $image);
            $validateData['image'] = $image;
            Certificate::create($validateData);

            return Redirect::route('certificate.show.all')->with('success', 'Successfully add certificate data!');
        } catch (\Exception $e) {
            return Redirect::route('certificate.create')->with('error', $e->getMessage());
        }
    }

    public function edit(Certificate $certificate)
    {
        $this->authorize('update', $certificate);

        return view('cv.admin.certificate.edit', ['certificate' => $certificate]);
    }

    public function update(UpdateCertificateRequest $request, Certificate $certificate)
    {
        try {
            $this->authorize('update', $certificate);

            $validateData = $request->validated();
            if ($request->image) {
                $image = uniqid() . '_' . $request->image->getClientOriginalName();
                $request->image->move(public_path('storage/images'), $image);
                $validateData['image'] = $image;
            }

            $certificate->update($validateData);

            return Redirect::route('certificate.show.all')->with('success', 'Successfully update certificate data!');
        } catch (\Exception $e) {
            return Redirect::route('certificate.edit', $certificate->id)->with('error', $e->getMessage());
        }
    }
}
