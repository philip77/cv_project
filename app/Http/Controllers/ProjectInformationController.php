<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectInformationRequest;
use App\Http\Requests\UpdateProjectInformationRequest;
use App\Models\Project;
use App\Models\ProjectInformation;
use Illuminate\Support\Facades\Redirect;

class ProjectInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function showAll(Project $project)
    {
        $this->authorize('viewAny', ProjectInformation::class);

        $projectInformationRows = ProjectInformation::where('project_id', $project->id)->orderBy('id', 'ASC')->get();

        return view('cv.admin.project-information.show-all', ['projectInformationRows' => $projectInformationRows, 'project' => $project]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Project $project)
    {
        $this->authorize('create', ProjectInformation::class);

        return view('cv.admin.project-information.create', ['project' => $project]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectInformationRequest $request)
    {
        try {
            $this->authorize('create', ProjectInformation::class);
            $validateData = $request->validated();

            ProjectInformation::create($validateData);

            return Redirect::route('project.information.show.all', $validateData['project_id'])->with('success', 'Successfully add project information data!');
        } catch (\Exception $e) {
            return Redirect::route('project.information.create', $validateData['project_id'])->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProjectInformation $projectInformation)
    {
        $this->authorize('update', $projectInformation);

        return view('cv.admin.project-information.edit', ['projectInformation' => $projectInformation]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProjectInformationRequest $request, ProjectInformation $projectInformation)
    {
        try {
            $this->authorize('update', $projectInformation);

            $validateData = $request->validated();
            $projectInformation->update($validateData);

            return Redirect::route('project.information.show.all', $projectInformation->project_id)->with('success', 'Successfully update project information data!');
        } catch (\Exception $e) {
            return Redirect::route('project.information.edit', $projectInformation->id)->with('error', $e->getMessage());
        }
    }
}
