<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEducationRequest;
use App\Http\Requests\UpdateEducationRequest;
use App\Models\Education;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Redirect;

class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        SEOMeta::setTitle('My Education');

        $educations = Education::where('is_deleted', 0)->orderBy('id', 'DESC')->get();

        return view('cv.education-index', ['educations' => $educations]);
    }

    public function showAll()
    {
        $this->authorize('viewAny', Education::class);

        return view('cv.admin.education.show-all', ['educations' => Education::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('create', Education::class);

        return view('cv.admin.education.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEducationRequest $request)
    {
        try {
            $this->authorize('create', Education::class);
            $validateData = $request->validated();

            Education::create($validateData);

            return Redirect::route('education.show.all')->with('success', 'Successfully add education data!');
        } catch (\Exception $e) {
            return Redirect::route('education.create')->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Education $education)
    {
        $this->authorize('update', $education);

        return view('cv.admin.education.edit', ['education' => $education]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEducationRequest $request, Education $education)
    {
        try {
            $this->authorize('update', $education);

            $validateData = $request->validated();
            $education->update($validateData);

            return Redirect::route('education.show.all')->with('success', 'Successfully update education data!');
        } catch (\Exception $e) {
            return Redirect::route('education.edit', $education->id)->with('error', $e->getMessage());
        }
    }
}
